const fs = require("fs");
const limits = require("./constants.js");

let alertMessages = [];
let satelliteKeys = [];
const TIMEINTERVAL = 300000;
let TSTATcounter = 0;
let BATTcounter = 0;

// Puts the time string into proper format to parse it with Date.parse
function convertTime(timeString) {
  let date = timeString.split(" ")[0];
  let time = timeString.split(" ")[1];
  let formattedDate =
    date.substring(0, 4) +
    "-" +
    date.substring(4, 6) +
    "-" +
    date.substring(6, 8);
  return `${formattedDate}T${time}Z`;
}

function getFileData(file) {
  return fs.readFileSync(file).toString().trim().split("\n");
}

// splits the satellites into their unique ids to easier track the limits
// and returnsd the alerts in JSON
function getUniqueSatelliteData(dataArray) {
  let initialTime = Date.parse(dataArray[0].timestamp);
  satelliteKeys = [...new Set(dataArray.map((id) => id.satelliteId))];
  satelliteKeys.forEach((id) => {
    for (item of dataArray) {
      if (item.satelliteId === id) {
        let rawtime = Date.parse(item.timestamp);
        if (rawtime < initialTime + TIMEINTERVAL) {
          if (
            item.component === "TSTAT" &&
            item.raw_value > limits.thermostat.RED_HIGH
          ) {
            TSTATcounter++;
            if (TSTATcounter === 3) {
              let tempOBJ = {
                satelliteId: item.satelliteId,
                severity: "RED_HIGH",
                component: item.component,
                timestamp: item.timestamp,
              };
              alertMessages.push(tempOBJ);
              TSTATcounter = 0;
            }
          }
          if (
            item.component === "BATT" &&
            item.raw_value < limits.battery.RED_LOW
          ) {
            BATTcounter++;
            console.log("Battery: ", BATTcounter);
            if (BATTcounter === 3) {
              let tempOBJ = {
                satelliteId: item.satelliteId,
                severity: "RED_LOW",
                component: item.component,
                timestamp: item.timestamp,
              };
              console.log(`BATT ${BATTcounter} Errors detected, resetting`);
              alertMessages.push(tempOBJ);
              BATTcounter = 0;
            }
          }
        } else if (rawtime > initialTime + TIMEINTERVAL) {
          initialTime = initialTime + TIMEINTERVAL;
        }
      }
    }
  });
  return alertMessages;
}

module.exports = {
  convertTime: convertTime,
  getFileData: getFileData,
  getUniqueSatelliteData: getUniqueSatelliteData,
};

module.exports = {
  battery: {
    RED_HIGH: 17,
    YELLOW_HIGH: 15,
    RED_LOW: 8,
    YELLOW_LOW: 9,
  },
  thermostat: {
    RED_HIGH: 101,
    YELLOW_HIGH: 90,
    RED_LOW: 20,
    YELLOW_LOW: 25,
  },
};

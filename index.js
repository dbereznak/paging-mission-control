const helpers = require("./helpers.js");
const fileName = process.argv.slice(2).toString();

let rawDataArray = [];
let finalDataArray = [];

if (!fileName) {
  rawDataArray = helpers.getFileData("data.txt");
} else {
  rawDataArray = helpers.getFileData(fileName);
}

rawDataArray.forEach((item) => {
  let tempArray = item.split("|");
  let tempOBJ = {};
  tempOBJ["satelliteId"] = tempArray[1];
  tempOBJ["raw_value"] = tempArray[6];
  tempOBJ["component"] = tempArray[7].substring(0, tempArray[7].length).trim();
  tempOBJ["timestamp"] = helpers.convertTime(tempArray[0]);
  finalDataArray.push(tempOBJ);
});

let alertJSON = helpers.getUniqueSatelliteData(finalDataArray);

console.log(alertJSON);
